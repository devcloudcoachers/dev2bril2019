trigger triggerAccount on Account (before insert,
                                    before update,
                                    before delete,
                                    after insert,
                                    after update,
                                    after delete,
                                    after undelete) {
                                        
    if(trigger.isAfter){
        if(trigger.isInsert){
            //AccountTriggerHandlerWhat3Words.insertShipping3Words(trigger.new);
            AccountTriggerHandlerWhat3Words.insertShipping3WordsQueue(trigger.new);
        }else if(trigger.isupdate){
            AccountTriggerHandlerWhat3Words.updateShipping3Words(trigger.old,trigger.new);
        }
    }
}