@IsTest
public class ResponseConvertTo3waError_Test {
	
	static testMethod void testParse() {
		String json = '{'+
		'    \"error\": {'+
		'        \"code\": \"BadCoordinates\",'+
		'        \"message\": \"failed to parse one of the coordinates as a valid decimal number\"'+
		'    }'+
		'}';
		ResponseConvertTo3waError obj = ResponseConvertTo3waError.parse(json);
		System.assert(obj != null);
	}
}