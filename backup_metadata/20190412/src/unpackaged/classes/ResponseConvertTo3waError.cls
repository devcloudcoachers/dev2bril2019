public class ResponseConvertTo3waError {

	public Error error;

	public class Error {
		public String code;
		public String message;
	}

	
	public static ResponseConvertTo3waError parse(String json) {
		return (ResponseConvertTo3waError) System.JSON.deserialize(json, ResponseConvertTo3waError.class);
	}
}