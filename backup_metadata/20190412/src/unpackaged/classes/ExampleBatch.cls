global without sharing class ExampleBatch implements Database.Batchable<sObject>, 
                                                     Database.Stateful{
    global Integer numbersRecordsUpdated = 0;
    global Integer numbersRecordsUndefined = 0;

    global Database.QueryLocator start(Database.BatchableContext bc)
    {  
        System.debug('START: ' + bc);
       return Database.getQueryLocator('SELECT Id, Name, Industry FROM Account');
    }

    global void execute(Database.BatchableContext bc, List<Account> records){
        System.debug('EXECUTE: ' + bc);
        System.debug('Updated records: ' + numbersRecordsUpdated);
        System.debug('Undefined records : ' + numbersRecordsUndefined);
        List<Account> lstAccountsToUpdate = new List<Account>();
        for(Account acc : records){
           if(String.isBlank(acc.Industry)){
               acc.Industry = 'Undefined';
               lstAccountsToUpdate.add(acc);
               numbersRecordsUpdated ++;
           }else if(acc.Industry == 'Undefined'){
               numbersRecordsUndefined++;
           }
        }
       update lstAccountsToUpdate;
    }   

    global void finish(Database.BatchableContext bc){
        System.debug('FINISH: ' + bc);
        // execute any post-processing operations
        System.debug('numbersRecordsUpdated: ' + numbersRecordsUpdated);
        System.debug('numbersRecordsUndefined: ' + numbersRecordsUndefined);
        System.debug('Total undefined Account: ' + (numbersRecordsUpdated+numbersRecordsUndefined));

	}
}