public with sharing class triggerLeadHandler {
    public static void convertLeadsFromWebToLead(list<Lead> lstNew) {
        for (Lead l : lstNew) {
            if(l.LeadSource == 'Web'){
                convertlead(l.Id);
            }
        }
    }

    public static void convertLeadsFromWebToLeadQueue(list<Lead> lstNew) {
        ConvertWebLeads_Queue queue = new ConvertWebLeads_Queue();
        for (Lead l : lstNew) {
            if(l.LeadSource == 'Web'){
                queue.add(l);
            }
        }
        if(queue.size()>0){
            System.enqueueJob(queue);
        }
    }

    @future(callout=true)
    private static void callout(){
        getListDogBreeds();
    }

    private static HttpResponse getListDogBreeds() {
       HttpRequest req = new HttpRequest();
       req.setEndpoint('https://dog.ceo/api/breeds/list/all');
       req.setMethod('GET');
       Http h = new Http();
       HttpResponse res = h.send(req);
       System.debug(res);
       return res;
   }


    @future
    private static void convertlead(Id sfid){
        //try{

            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(sfid);

            lc.setConvertedStatus('Closed - Converted');

            Database.LeadConvertResult lcr = Database.convertLead(lc);
            if(lcr.isSuccess()){
                System.debug('lead converted');
            }else{
                System.debug(System.LoggingLevel.ERROR, 'Error al convertir');
            }
        /*}catch(Exception ex){
            Lead l = new Lead();
            l.Id = sfid;
            l.Error__c = true;
            l.AsyncErrorMessage__c = ex.getMessage();
            update l;
        }*/
        
    }
}