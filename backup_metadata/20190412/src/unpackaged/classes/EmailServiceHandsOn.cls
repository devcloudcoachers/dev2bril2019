global class EmailServiceHandsOn implements Messaging.InboundEmailHandler{
    global Messaging.InboundEmailResult handleInboundEmail(
                Messaging.InboundEmail email, 
                Messaging.InboundEnvelope envelope) {
		Messaging.InboundEmailResult result;
        result = new Messaging.InboundEmailresult();
        try{
            List<List<SObject>> lstlstSObject = [FIND :email.fromAddress IN EMAIL FIELDS
                                                RETURNING Lead,Contact];
            Task t = new Task();
            t.Subject = email.subject;
            t.ActivityDate = Date.Today().addDays(7);
            t.status = 'Not Started';
            t.Description = email.htmlBody;
            
            if(lstlstSObject[0].size()==0){
                if(lstlstSObject[1].size()==0){
                    Lead lead = new Lead(LastName = email.fromname,
                                        email = email.fromAddress);
                    insert lead;
                    t.WhoId = lead.Id;
                }else{
                    SObject sobj = lstlstSObject[1][0];
                    t.WhoId = sobj.Id;
                }
            }else{
                SObject sobj = lstlstSObject[0][0];
                t.WhoId = sobj.Id;
            }
            insert t;
        }catch(Exception ex){
            result.success = false;
            result.message = ex.getMessage();
        }
        result.success = true;
        return result;
      }
  }