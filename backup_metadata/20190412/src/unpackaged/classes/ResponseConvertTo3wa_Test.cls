//
// Generated by JSON2Apex http://json2apex.herokuapp.com/
//

@IsTest
public class ResponseConvertTo3wa_Test {
	
	// This test method should give 100% coverage
	static testMethod void testParse() {
		String json = '{\"country\":\"GB\",\"square\":{\"southwest\":{\"lng\":-0.203607,\"lat\":51.521238},\"northeast\":{\"lng\":-0.203564,\"lat\":51.521265}},\"nearestPlace\":\"Bayswater, London\",\"coordinates\":{\"lng\":-0.203586,\"lat\":51.521251},\"words\":\"index.home.raft\",\"language\":\"en\",\"map\":\"https:\\/\\/w3w.co\\/index.home.raft\"}';
		ResponseConvertTo3wa r = ResponseConvertTo3wa.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		ResponseConvertTo3wa.Southwest objSouthwest = new ResponseConvertTo3wa.Southwest(System.JSON.createParser(json));
		System.assert(objSouthwest != null);
		System.assert(objSouthwest.lng == null);
		System.assert(objSouthwest.lat == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		ResponseConvertTo3wa objResponseConvertTo3wa = new ResponseConvertTo3wa(System.JSON.createParser(json));
		System.assert(objResponseConvertTo3wa != null);
		System.assert(objResponseConvertTo3wa.country == null);
		System.assert(objResponseConvertTo3wa.square == null);
		System.assert(objResponseConvertTo3wa.nearestPlace == null);
		System.assert(objResponseConvertTo3wa.coordinates == null);
		System.assert(objResponseConvertTo3wa.words == null);
		System.assert(objResponseConvertTo3wa.language == null);
		System.assert(objResponseConvertTo3wa.map_Z == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		ResponseConvertTo3wa.Square objSquare = new ResponseConvertTo3wa.Square(System.JSON.createParser(json));
		System.assert(objSquare != null);
		System.assert(objSquare.southwest == null);
		System.assert(objSquare.northeast == null);
	}
}