@RestResource(urlMapping='/AccountAndContact/v1/*')
global with sharing class CC_RestWebService {
    
  
   @HttpPost
   global static ResponseIds createAccount(Account account, Contact contact) {
       ResponseIds resp = new ResponseIds();
       try{
           insert account;
	       contact.AccountId = account.Id;
    	   insert contact;
    	   resp.idAccount = account.Id;
	       resp.idContact = contact.Id;
           resp.success = true;
       }catch(Exception ex){
           resp.success = false;
           resp.message = ex.getMessage();
       }
       return resp;
   }
   
	global class ResponseIds{
        public Boolean success {get;set;}
        public String message {get;set;}
		public Id idAccount{get;set;}
		public Id idContact{get;set;}
	}
    
   @HttpGet
   global static ResponseGetRecords getRecords() {
       ResponseGetRecords response = new ResponseGetRecords();
       try{
           response.account = [SELECT Id,Name FROM Account];
           response.success = true;
       }catch(Exception ex){
           RestContext.response.statusCode = 500;
           response.success = false;
           response.message = ex.getMessage();
       }
       return response;
   }
    
    global class ResponseGetRecords{
        public Boolean success {get;set;}
        public String message {get;set;}
        public Account account {get;set;}
        public Integer size {get;set;}
    }
}