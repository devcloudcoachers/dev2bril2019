global class CC_SoapWebService {
   webservice static Id createContact(String contactLastName, Account a) {
       Contact c = new Contact(lastName = contactLastName, AccountId = a.Id);
       insert c;
       return c.id;
   }
}