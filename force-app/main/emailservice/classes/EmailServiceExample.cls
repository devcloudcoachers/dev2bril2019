global class EmailServiceExample implements Messaging.InboundEmailHandler{
    global Messaging.InboundEmailResult handleInboundEmail(
                Messaging.InboundEmail email, 
                Messaging.InboundEnvelope envelope) {
		Messaging.InboundEmailResult result;
        result = new Messaging.InboundEmailresult();
        System.debug('email: ' + email);
        System.debug('envelope: ' + envelope);
        Account account = new Account(Name = email.fromname);
        insert account;
        result.success = true;
        return result;
      }
  }