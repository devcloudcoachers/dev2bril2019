public with sharing class AccountTriggerHandlerWhat3Words {
    public static void updateShipping3Words(List<Account> lstOld,List<Account> lstNew) {
        for(Integer i = 0;i<lstNew.size();i++){
            if(lstOld[i].ShippingLatitude!=lstNew[i].ShippingLatitude || 
               lstOld[i].ShippingLongitude!=lstNew[i].ShippingLongitude){

                   asyncUpdateShipping3Word(lstNew[i].Id,lstNew[i].ShippingLatitude,lstNew[i].ShippingLongitude);
            }
        }
    }

    public static void insertShipping3Words(List<Account> lstNew) {
        List<Id> lstIds = new List<Id>();
        List<Decimal> lstLatitude = new List<Decimal>();
        List<Decimal> lstLongitude = new List<Decimal>();
        for(Account acc : lstNew){
            if(acc.ShippingLatitude!=null && acc.ShippingLongitude !=null){
                lstIds.add(acc.Id);
                lstLatitude.add(acc.ShippingLatitude);
                lstLongitude.add(acc.ShippingLongitude);
                if(lstIds.size()==50){
                    asyncUpdateShipping3Word(lstIds,lstLatitude,lstLongitude);
                    lstIds.clear();
                    lstLatitude.clear();
                    lstLongitude.clear();
                }
                // Error por limite 51 futures methods
                // asyncUpdateShipping3Word(acc.Id,acc.ShippingLatitude,acc.ShippingLongitude);
            }
        }
        if(lstIds.size()>0){
            asyncUpdateShipping3Word(lstIds,lstLatitude,lstLongitude);
        }
    }

    public static void insertShipping3WordsQueue(List<Account> lstNew) {
        What3WordsShipping_Queue updateJob = new What3WordsShipping_Queue();
        for(Account acc : lstNew){
            if(acc.ShippingLatitude!=null && acc.ShippingLongitude !=null){
                updateJob.add(acc);
                if(updateJob.size()==50){
                    ID jobID = System.enqueueJob(updateJob);
                    updateJob = new What3WordsShipping_Queue();
                }
            }
        }
        if(updateJob.size()>0){
            ID jobID = System.enqueueJob(updateJob);
        }
    }

    @future(callout=true)
    private static void asyncUpdateShipping3Word(Id sfid, Decimal latitude, Decimal longitude){
        Account acc = new Account(Id = sfid);
        String words = CC_CalloutUtil.getWhat3WordsFromGeolocation(latitude,longitude);
        acc.Shipping3Words__c = words;
        update acc;
    }

    @future(callout=true)
    private static void asyncUpdateShipping3Word(   List<Id> lstIds, 
                                                    List<Decimal> lstLatitude, 
                                                    List<Decimal> lstLongitude){
        List<Account> lstAccounts = new List<Account>();
        for(Integer i = 0; i<lstIds.size(); i++){
            Account acc = new Account(Id = lstIds[i]);
            String words = CC_CalloutUtil.getWhat3WordsFromGeolocation(lstLatitude[i],lstLongitude[i]);
            acc.Shipping3Words__c = words;
            lstAccounts.add(acc);
        }
        update lstAccounts;
        
    }
}
