/**
    @description Clase para realizar la llamada a el web service de What2Words
    https://docs.what3words.com/api/v3/#convert-to-3wa
 */
    public with sharing class CC_CalloutUtil {
    
    public static String getWhat3WordsFromGeolocation(Decimal latitude, Decimal longitude){
        String returnString = '';

        What3WordsConfig__c config = What3WordsConfig__c.getInstance();

        HttpRequest req = new HttpRequest();
        req.setEndpoint(config.DomainServer__c + 'v3/convert-to-3wa?coordinates=' + Latitude + ',' + Longitude + '&key=' + config.AuthKey__c);
        req.setMethod('GET');
        Http h = new Http();
        HttpResponse res = h.send(req);

        if(res.getStatusCode()==200){
            ResponseConvertTo3wa responseOK = ResponseConvertTo3wa.parse(res.getBody());
            returnString = responseOK.words;
        }else{
            ResponseConvertTo3waError responseError = ResponseConvertTo3waError.parse(res.getBody());
            throw new What3WordsRequestException(responseError.error.code + ' - ' + responseError.error.message);
        }
        System.debug(returnString);
        return returnString;
    }

    private class What3WordsRequestException extends Exception {}

}
