public with sharing class What3WordsShipping_Queue implements Queueable,Database.AllowsCallouts  {
    private List<Account> lstAccounts;
    
    public What3WordsShipping_Queue() {
        lstAccounts = new List<Account>();
    }

    public void add(Account account){
        Account acc = new Account();
        acc.Id=account.Id;
        acc.ShippingLatitude=account.ShippingLatitude;
        acc.ShippingLongitude=account.ShippingLongitude;
        lstAccounts.add(acc);
    }

    public Integer size(){
        return lstAccounts.size();
    }
    
    public void execute(QueueableContext context) {
    	for(Integer i = 0; i<lstAccounts.size(); i++){
            String words = CC_CalloutUtil.getWhat3WordsFromGeolocation(lstAccounts[i].ShippingLatitude,lstAccounts[i].ShippingLongitude);
            lstAccounts[i].Shipping3Words__c = words;
        }
        update this.lstAccounts;
    }
}

