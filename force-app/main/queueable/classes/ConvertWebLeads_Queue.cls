public with sharing class ConvertWebLeads_Queue implements Queueable{
    private List<Lead> lstLeads;
    private List<Lead> leadWithErrors;
    
    public ConvertWebLeads_Queue() {
        lstLeads = new  List<Lead>();
    }

    public void add(Lead lead){
        lstLeads.add(lead);
    }

    public Integer size(){
        return lstLeads.size();
    }
    
    public void execute(QueueableContext context) {
        leadWithErrors = new List<Lead>();
    	for(Integer i = 0; i<lstLeads.size(); i++){
            try{
                Database.LeadConvert lc = new Database.LeadConvert();
                lc.setLeadId(lstLeads[i].Id);

                lc.setConvertedStatus('Closed - Converted');

                Database.LeadConvertResult lcr = Database.convertLead(lc);
                if(lcr.isSuccess()){
                    System.debug('lead converted');
                }else{
                    System.debug(System.LoggingLevel.ERROR, 'Error al convertir');
                }
            }catch(Exception ex){
                Lead l = new Lead();
                l.Id = lstLeads[i].Id;
                l.Error__c = true;
                l.AsyncErrorMessage__c = ex.getMessage();
                update l;
            }
        }

        if(leadWithErrors.size()<0){
            update leadWithErrors;
        }
    }
}