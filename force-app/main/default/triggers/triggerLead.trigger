trigger triggerLead on Lead (after insert) {
    if(trigger.isAfter){
        if(trigger.isInsert){
            //triggerLeadHandler.convertLeadsFromWebToLead(trigger.new);
            triggerLeadHandler.convertLeadsFromWebToLeadQueue(trigger.new);
        }
    }
}